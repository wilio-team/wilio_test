FactoryBot.define do
  factory :user do
    email { Faker::Internet.unique.safe_email }
  end
end
