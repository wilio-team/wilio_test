require 'rails_helper'

describe Types::QueryType do
  let(:user1) { create(:user) }
  let(:user2) { create(:user) }

  def graphql_execute(query, context = {}, variables = {})
    WilioTestSchema.execute(query, context: context, variables: variables.deep_stringify_keys)
      .to_h
      .deep_symbolize_keys
      .tap do |result|
      expect(result.to_h.fetch(:errors, [])).to be_empty
    end
  end

  describe 'chat_messages' do
    let(:message) { 'Hello john.wick@gmail.com, nice to meet u.' }

    before do
      graphql_execute(
        %| mutation($message: String!, $senderId: Int!, $recipientId: Int!) { sendChat(senderId: $senderId, recipientId: $recipientId, message: $message) { message } } |,
        nil,
        senderId: user1.id,
        recipientId: user2.id,
        message: message,
      )
    end

    subject do
      graphql_execute(
        %| query($id: Int!) { user(id: $id) { messages { id, message, sender { id, email}, recipient { id, email } } } } |,
        {},
        id: user1.id,
      )
    end

    it 'anonymizes chat message' do
      expect(subject[:data][:user][:messages].length).to eq 1
      expect(subject[:data][:user][:messages][0][:sender][:id]).to eq user1.id
      expect(subject[:data][:user][:messages][0][:recipient][:id]).to eq user2.id
      expect(subject[:data][:user][:messages][0][:message]).to eq 'Hello j*****@g*****.com, nice to meet u.'
    end
  end

  describe 'email_messages' do
    let(:message) { 'Call me on +421 904 443 234 at 13:30.' }

    before do
      graphql_execute(
        %| mutation($message: String!, $senderId: Int!, $recipientId: Int!) { sendEmail(senderId: $senderId, recipientId: $recipientId, message: $message) { message } } |,
        nil,
        senderId: user1.id,
        recipientId: user2.id,
        message: message,
      )
    end

    subject do
      graphql_execute(
        %| query($id: Int!) { user(id: $id) { messages { id, message, sender { id, email}, recipient { id, email } } } } |,
        {},
        id: user1.id,
      )
    end

    it 'anonymizes chat message' do
      expect(subject[:data][:user][:messages].length).to eq 1
      expect(subject[:data][:user][:messages][0][:sender][:id]).to eq user1.id
      expect(subject[:data][:user][:messages][0][:recipient][:id]).to eq user2.id
      expect(subject[:data][:user][:messages][0][:message]).to eq 'Call me on +421 *** *** *** at 13:30.'
    end
  end

  describe 'generic_messages' do
    let(:message) { 'Hello john.wick@gmail.com, nice to meet u. Call me on +421 904 443 234 at 13:30.' }

    before do
      graphql_execute(
        %| mutation($message: String!, $senderId: Int!, $recipientId: Int!) { sendMessage(senderId: $senderId, recipientId: $recipientId, message: $message) { message } } |,
        nil,
        senderId: user1.id,
        recipientId: user2.id,
        message: message,
      )
    end

    subject do
      graphql_execute(
        %| query($id: Int!) { user(id: $id) { messages { id, message, sender { id, email}, recipient { id, email } } } } |,
        {},
        id: user2.id,
      )
    end

    it 'anonymizes chat message' do
      expect(subject[:data][:user][:messages].length).to eq 1
      expect(subject[:data][:user][:messages][0][:sender][:id]).to eq user1.id
      expect(subject[:data][:user][:messages][0][:recipient][:id]).to eq user2.id
      expect(subject[:data][:user][:messages][0][:message]).to eq 'Hello j*****@g*****.com, nice to meet u. Call me on +421 *** *** *** at 13:30.'
    end
  end
end
