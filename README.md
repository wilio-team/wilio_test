# Wilio interview task

## Overview

Create a backend chat app that anonymizes personal data

## Goals
- Get familiar with rails
- Learn clean code
- Learn GraphQL
- Get familiar with tests

## Specifications

Create a backend chat application that anonymizes personal information.

- Users have only email addresses 
- Chat messages have only text.

### Scenario 1
User A sends a chat message to user B. Chat message cannot contain email address. Keep only first letter, add 5 stars, keep @, keep 1st letter of domain, add 5 stars, keep .com (or .sk, etc.)

Example:

Input: `Hello john.wick@gmail.com, nice to meet u.`

Output: `Hello j*****@g*****.com, nice to meet u.`

### Scenario 2

User A sends an email message to user B. Email message cannot contain phone number. Replace phone numbers with stars. Keep only prefix (0 or +421)

Input: `Call me on +421 904 443 234 at 13:30.`

Output: `Call me on +421 *** *** *** at 13:30.`

### Scenario 3

User A sends a generic text message to user B that anonymizes phone numbers and email addresses according to scenario 1 and 2. 

### Validity test

`rspec spec` should pass all tests. Feel free to edit `spec/graphql/query_type_spec.rb` test according to your needs.
Maybe we put there a mistake.

### Hints

- Use graphql: https://graphql-ruby.org/getting_started
- regex can be useful https://medium.com/factory-mind/regex-tutorial-a-simple-cheatsheet-by-examples-649dc1c3f285
- `rails c` starts debug console
- rubymine has debug/run mode for test (free for students/trial)

### Bonus points
Do sth more for bonus points 
